import Vue from 'vue'
import Router from 'vue-router'
import Products from '../components/Products.vue'
import About from '../components/About.vue'
import Login from '../components/Login.vue'
import Register from '../components/Register.vue'
import ProductInformation from '../components/ProductInformation.vue'
import Order from '../components/Checkout.vue'
import ProductList from '../components/ProductList.vue'
import AllProducts from "@/components/AllProducts"
import AddProduct from "../components/AddProduct";

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'products',
            component: Products
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/products/:productId',
            name: 'productInformation',
            component: ProductInformation
        },
        {
            path: '/order',
            name: 'order',
            component: Order
        },
        {
            path: '/productList',
            name: 'productList',
            component: ProductList
        },
        {
            path: '/allProducts',
            name: 'allProducts',
            component: AllProducts
        }
        ,
        {
            path: '/addProduct',
            name: 'addProduct',
            component: AddProduct
        }
    ],
    Router: Router
})