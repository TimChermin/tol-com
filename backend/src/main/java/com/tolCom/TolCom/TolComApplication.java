package com.tolCom.TolCom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TolComApplication {

	public static void main(String[] args) {
		SpringApplication.run(TolComApplication.class, args);
	}

}
